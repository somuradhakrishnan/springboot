package com.myspace.demo.config.batch;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.integration.chunk.ChunkProcessorChunkHandler;
import org.springframework.batch.integration.chunk.ChunkRequest;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * Created by ayyanars on 22/4/17.
 */
@Configuration
public class BatchConfiguration {

    private static final Log logger = LogFactory.getLog(BatchConfiguration.class);

    @Bean
    public ItemWriter<String> itemWriter(){
        ItemWriter<String> processItem = (List<? extends String> list) -> {};
        return processItem;
    }
}

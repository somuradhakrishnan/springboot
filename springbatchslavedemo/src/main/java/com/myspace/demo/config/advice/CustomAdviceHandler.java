package com.myspace.demo.config.advice;

import org.springframework.batch.integration.chunk.ChunkRequest;
import org.springframework.integration.handler.advice.AbstractRequestHandlerAdvice;
import org.springframework.messaging.Message;

import java.util.Collection;
import java.util.List;

/**
 * Created by ayyanars on 2/5/17.
 */
public class CustomAdviceHandler extends AbstractRequestHandlerAdvice {
    @Override
    protected Object doInvoke(ExecutionCallback executionCallback, Object o, Message<?> message) throws Exception {
        ChunkRequest chunkRequest = (ChunkRequest)message.getPayload();
        Collection<String> data = chunkRequest.getItems();

        System.out.println(">>>>>Test>>>");

        Object result = executionCallback.execute();
        return result;
    }
}

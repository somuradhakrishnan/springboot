package com.myspace.demo;

import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.stream.Stream;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class SpringbatchApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void testStream(){
		try{

			System.out.println(">>>>>>>>>>>1"+new Date());
			Path file = Paths.get(ClassLoader.getSystemResource("sample-data1.csv").toURI());
			Stream<String> lines = Files.lines( file, StandardCharsets.UTF_8 );
			System.out.println(">>>>>>>>>>>2"+new Date());
			for( String line : (Iterable<String>) lines::iterator )
			{
				//System.out.println(line);
			}

			System.out.println(">>>>>>>>>>>3"+new Date());

		}catch (IOException | URISyntaxException e){
			e.printStackTrace();
		}
	}

}

package com.myspace.demo.controllers;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by ayyanars on 22/4/17.
 */
@RestController
public class ReqReceiverControllers {

    @Autowired
    JobLauncher jobLauncher;

    @Autowired
    Job job;

    @RequestMapping("/launchJob")
    public void handle() throws Exception{
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString("dest", "Testing Job Parameter");
        jobParametersBuilder.addLong("Current_time",System.currentTimeMillis());
        jobLauncher.run(job, jobParametersBuilder.toJobParameters());
    }

}

package com.myspace.demo.config.mq;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.messaging.MessageChannel;

import javax.jms.Queue;

/**
 * Created by ayyanars on 16/4/17.
 */
@Configuration
public class mqConfiguration {

    @Autowired
    private MessageChannel jmsRequests;

    @Value("${mq.request.queue}")
    private String queueName;

    @Bean
    public MessagingTemplate messagingGateway(){
        MessagingTemplate messagingTemplate = new MessagingTemplate(jmsRequests);
        messagingTemplate.setReceiveTimeout(5000);
        return  messagingTemplate;
    }

    @Bean
    public Queue testJMSQueue() {
        return new ActiveMQQueue(queueName);
    }

 }

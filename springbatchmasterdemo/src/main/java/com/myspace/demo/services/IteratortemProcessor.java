package com.myspace.demo.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

/**
 * Created by ayyanars on 21/3/17.
 */
public class IteratortemProcessor implements ItemProcessor<String, String> {

    private static final Logger log = LoggerFactory.getLogger(IteratortemProcessor.class);

    @Override
    public String process(final String string) throws Exception {

        final String transformedstream = string;

        log.info("Converting (" + string + ") into (" + string + ")");

        return transformedstream;
    }
}

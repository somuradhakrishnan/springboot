package com.myspace.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.integration.config.EnableIntegration;

@SpringBootApplication
@EnableIntegration
@ImportResource("master-context.xml")
public class SpringbatchmasterdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbatchmasterdemoApplication.class, args);
	}
}

package com.dbs.itt.sdwt.model.documents;

/**
 * Created by ayyanars on 10/12/17.
 */
public class SecondaryAddresses {

    private String otherIdNumber;

    private String isEnabled;

    private String type;

    private String otherAddress1;

    private String otherAddress2;

    private String otherAddress3;

    private String otherAddress4;

    private String otherCity;

    private String otherZipCode;

    private String otherCountry;

    private String otherTel;

    private String otherFax;

    private String otherSwift;

    private String otherTelex;

    private String otherEmail;

    private String contact1;

    private String contact2;

    private String otherComment1;

    private String otherComment2;

    private String otherComment3;

    private String otherComment4;

    public String getOtherIdNumber() {
        return otherIdNumber;
    }

    public void setOtherIdNumber(String otherIdNumber) {
        this.otherIdNumber = otherIdNumber;
    }

    public String getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(String isEnabled) {
        this.isEnabled = isEnabled;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOtherAddress1() {
        return otherAddress1;
    }

    public void setOtherAddress1(String otherAddress1) {
        this.otherAddress1 = otherAddress1;
    }

    public String getOtherAddress2() {
        return otherAddress2;
    }

    public void setOtherAddress2(String otherAddress2) {
        this.otherAddress2 = otherAddress2;
    }

    public String getOtherAddress3() {
        return otherAddress3;
    }

    public void setOtherAddress3(String otherAddress3) {
        this.otherAddress3 = otherAddress3;
    }

    public String getOtherAddress4() {
        return otherAddress4;
    }

    public void setOtherAddress4(String otherAddress4) {
        this.otherAddress4 = otherAddress4;
    }

    public String getOtherCity() {
        return otherCity;
    }

    public void setOtherCity(String otherCity) {
        this.otherCity = otherCity;
    }

    public String getOtherZipCode() {
        return otherZipCode;
    }

    public void setOtherZipCode(String otherZipCode) {
        this.otherZipCode = otherZipCode;
    }

    public String getOtherCountry() {
        return otherCountry;
    }

    public void setOtherCountry(String otherCountry) {
        this.otherCountry = otherCountry;
    }

    public String getOtherTel() {
        return otherTel;
    }

    public void setOtherTel(String otherTel) {
        this.otherTel = otherTel;
    }

    public String getOtherFax() {
        return otherFax;
    }

    public void setOtherFax(String otherFax) {
        this.otherFax = otherFax;
    }

    public String getOtherSwift() {
        return otherSwift;
    }

    public void setOtherSwift(String otherSwift) {
        this.otherSwift = otherSwift;
    }

    public String getOtherTelex() {
        return otherTelex;
    }

    public void setOtherTelex(String otherTelex) {
        this.otherTelex = otherTelex;
    }

    public String getOtherEmail() {
        return otherEmail;
    }

    public void setOtherEmail(String otherEmail) {
        this.otherEmail = otherEmail;
    }

    public String getContact1() {
        return contact1;
    }

    public void setContact1(String contact1) {
        this.contact1 = contact1;
    }

    public String getContact2() {
        return contact2;
    }

    public void setContact2(String contact2) {
        this.contact2 = contact2;
    }

    public String getOtherComment1() {
        return otherComment1;
    }

    public void setOtherComment1(String otherComment1) {
        this.otherComment1 = otherComment1;
    }

    public String getOtherComment2() {
        return otherComment2;
    }

    public void setOtherComment2(String otherComment2) {
        this.otherComment2 = otherComment2;
    }

    public String getOtherComment3() {
        return otherComment3;
    }

    public void setOtherComment3(String otherComment3) {
        this.otherComment3 = otherComment3;
    }

    public String getOtherComment4() {
        return otherComment4;
    }

    public void setOtherComment4(String otherComment4) {
        this.otherComment4 = otherComment4;
    }
}

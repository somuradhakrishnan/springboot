package com.dbs.itt.sdwt.model.documents;

/**
 * Created by ayyanars on 10/12/17.
 */
public class Mlc {

    private String mlcParentLabel;

    private String mlcChildLabel;

    private String mlcType;

    private String mlcStatus;

    private String mlcCustomerOwner;

    private String mlcRingFencedFlag;

    private String mlcOwnBankFlag;

    private String ultimateParent;

    private String mlcIsdaFlag;

    private String mlcCsaFlag;

    private String mlcChalkSrFlag;

    private String facilityCodeSharing;

    public String getMlcParentLabel() {
        return mlcParentLabel;
    }

    public void setMlcParentLabel(String mlcParentLabel) {
        this.mlcParentLabel = mlcParentLabel;
    }

    public String getMlcChildLabel() {
        return mlcChildLabel;
    }

    public void setMlcChildLabel(String mlcChildLabel) {
        this.mlcChildLabel = mlcChildLabel;
    }

    public String getMlcType() {
        return mlcType;
    }

    public void setMlcType(String mlcType) {
        this.mlcType = mlcType;
    }

    public String getMlcStatus() {
        return mlcStatus;
    }

    public void setMlcStatus(String mlcStatus) {
        this.mlcStatus = mlcStatus;
    }

    public String getMlcCustomerOwner() {
        return mlcCustomerOwner;
    }

    public void setMlcCustomerOwner(String mlcCustomerOwner) {
        this.mlcCustomerOwner = mlcCustomerOwner;
    }

    public String getMlcRingFencedFlag() {
        return mlcRingFencedFlag;
    }

    public void setMlcRingFencedFlag(String mlcRingFencedFlag) {
        this.mlcRingFencedFlag = mlcRingFencedFlag;
    }

    public String getMlcOwnBankFlag() {
        return mlcOwnBankFlag;
    }

    public void setMlcOwnBankFlag(String mlcOwnBankFlag) {
        this.mlcOwnBankFlag = mlcOwnBankFlag;
    }

    public String getUltimateParent() {
        return ultimateParent;
    }

    public void setUltimateParent(String ultimateParent) {
        this.ultimateParent = ultimateParent;
    }

    public String getMlcIsdaFlag() {
        return mlcIsdaFlag;
    }

    public void setMlcIsdaFlag(String mlcIsdaFlag) {
        this.mlcIsdaFlag = mlcIsdaFlag;
    }

    public String getMlcCsaFlag() {
        return mlcCsaFlag;
    }

    public void setMlcCsaFlag(String mlcCsaFlag) {
        this.mlcCsaFlag = mlcCsaFlag;
    }

    public String getMlcChalkSrFlag() {
        return mlcChalkSrFlag;
    }

    public void setMlcChalkSrFlag(String mlcChalkSrFlag) {
        this.mlcChalkSrFlag = mlcChalkSrFlag;
    }

    public String getFacilityCodeSharing() {
        return facilityCodeSharing;
    }

    public void setFacilityCodeSharing(String facilityCodeSharing) {
        this.facilityCodeSharing = facilityCodeSharing;
    }
}

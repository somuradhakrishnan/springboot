package com.dbs.itt.sdwt.model.documents;

/**
 * Created by ayyanars on 10/12/17.
 */
public class Gcin {

    private String gcinNumber;

    private String gcinStatus;

    private String gcinCustomerType;

    private String gcinLocalRm;

    private String gcinCustomerOwner;

    private String gcinCustomerSegment;

    private String gcinCountryOfDomicile;

    private String gcinDateOfIncorporation;

    private String gcinConstitution;

    private String gcinDocumentType;

    private String gcinDocumentCode;

    private String gcinUniqueid;

    private String gcinCountryOfIssue;

    private String gcinIssueDate;

    private String refType;


}

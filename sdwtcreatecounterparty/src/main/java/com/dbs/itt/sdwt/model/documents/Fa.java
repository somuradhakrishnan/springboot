package com.dbs.itt.sdwt.model.documents;

import com.sun.xml.internal.ws.api.ha.StickyFeature;

/**
 * Created by ayyanars on 10/12/17.
 */
public class Fa {

    private String faLabel;

    private String faType;

    private String faStatus;

    private String faRedCode;

    private String faFree1;

    private String faFree2;

    private String faFree3;

    private String faFree4;

    private String faFree5;

    private String faCreditTermsCond;

    private String faDtccCompanyNbr;

    private String faEntityClip;

    private String faIndexAnnexDate;

    private String faIndexTrancheEffDate;

    private String faLei;

    private String faPrimaryObligor1;

    private String faPrimaryObligor2;

    private String faRecoveryRate;

    private String faRefEntity1;

    private String faRefEntity2;

    private String faRefObligation1;

    private String faRefObligation2;

    private String faRttDefaultDate;

    private String faRttDefaultRr;

    private String faS29;

    private String faSnrProxy;

    private String faSubProxy;

    private String faBisStatus;

    private String faBusinessStatus;

    private String faOblDefaultFlag;

    private String faS35Flag;

    private String faSpecificFlag;

    private String faStructuredSpecificFlag;

    private String faSnrProxy_;

    private String faSubProxy_;

    public String getFaLabel() {
        return faLabel;
    }

    public void setFaLabel(String faLabel) {
        this.faLabel = faLabel;
    }

    public String getFaType() {
        return faType;
    }

    public void setFaType(String faType) {
        this.faType = faType;
    }

    public String getFaStatus() {
        return faStatus;
    }

    public void setFaStatus(String faStatus) {
        this.faStatus = faStatus;
    }

    public String getFaRedCode() {
        return faRedCode;
    }

    public void setFaRedCode(String faRedCode) {
        this.faRedCode = faRedCode;
    }

    public String getFaFree1() {
        return faFree1;
    }

    public void setFaFree1(String faFree1) {
        this.faFree1 = faFree1;
    }

    public String getFaFree2() {
        return faFree2;
    }

    public void setFaFree2(String faFree2) {
        this.faFree2 = faFree2;
    }

    public String getFaFree3() {
        return faFree3;
    }

    public void setFaFree3(String faFree3) {
        this.faFree3 = faFree3;
    }

    public String getFaFree4() {
        return faFree4;
    }

    public void setFaFree4(String faFree4) {
        this.faFree4 = faFree4;
    }

    public String getFaFree5() {
        return faFree5;
    }

    public void setFaFree5(String faFree5) {
        this.faFree5 = faFree5;
    }

    public String getFaCreditTermsCond() {
        return faCreditTermsCond;
    }

    public void setFaCreditTermsCond(String faCreditTermsCond) {
        this.faCreditTermsCond = faCreditTermsCond;
    }

    public String getFaDtccCompanyNbr() {
        return faDtccCompanyNbr;
    }

    public void setFaDtccCompanyNbr(String faDtccCompanyNbr) {
        this.faDtccCompanyNbr = faDtccCompanyNbr;
    }

    public String getFaEntityClip() {
        return faEntityClip;
    }

    public void setFaEntityClip(String faEntityClip) {
        this.faEntityClip = faEntityClip;
    }

    public String getFaIndexAnnexDate() {
        return faIndexAnnexDate;
    }

    public void setFaIndexAnnexDate(String faIndexAnnexDate) {
        this.faIndexAnnexDate = faIndexAnnexDate;
    }

    public String getFaIndexTrancheEffDate() {
        return faIndexTrancheEffDate;
    }

    public void setFaIndexTrancheEffDate(String faIndexTrancheEffDate) {
        this.faIndexTrancheEffDate = faIndexTrancheEffDate;
    }

    public String getFaLei() {
        return faLei;
    }

    public void setFaLei(String faLei) {
        this.faLei = faLei;
    }

    public String getFaPrimaryObligor1() {
        return faPrimaryObligor1;
    }

    public void setFaPrimaryObligor1(String faPrimaryObligor1) {
        this.faPrimaryObligor1 = faPrimaryObligor1;
    }

    public String getFaPrimaryObligor2() {
        return faPrimaryObligor2;
    }

    public void setFaPrimaryObligor2(String faPrimaryObligor2) {
        this.faPrimaryObligor2 = faPrimaryObligor2;
    }

    public String getFaRecoveryRate() {
        return faRecoveryRate;
    }

    public void setFaRecoveryRate(String faRecoveryRate) {
        this.faRecoveryRate = faRecoveryRate;
    }

    public String getFaRefEntity1() {
        return faRefEntity1;
    }

    public void setFaRefEntity1(String faRefEntity1) {
        this.faRefEntity1 = faRefEntity1;
    }

    public String getFaRefEntity2() {
        return faRefEntity2;
    }

    public void setFaRefEntity2(String faRefEntity2) {
        this.faRefEntity2 = faRefEntity2;
    }

    public String getFaRefObligation1() {
        return faRefObligation1;
    }

    public void setFaRefObligation1(String faRefObligation1) {
        this.faRefObligation1 = faRefObligation1;
    }

    public String getFaRefObligation2() {
        return faRefObligation2;
    }

    public void setFaRefObligation2(String faRefObligation2) {
        this.faRefObligation2 = faRefObligation2;
    }

    public String getFaRttDefaultDate() {
        return faRttDefaultDate;
    }

    public void setFaRttDefaultDate(String faRttDefaultDate) {
        this.faRttDefaultDate = faRttDefaultDate;
    }

    public String getFaRttDefaultRr() {
        return faRttDefaultRr;
    }

    public void setFaRttDefaultRr(String faRttDefaultRr) {
        this.faRttDefaultRr = faRttDefaultRr;
    }

    public String getFaS29() {
        return faS29;
    }

    public void setFaS29(String faS29) {
        this.faS29 = faS29;
    }

    public String getFaSnrProxy() {
        return faSnrProxy;
    }

    public void setFaSnrProxy(String faSnrProxy) {
        this.faSnrProxy = faSnrProxy;
    }

    public String getFaSubProxy() {
        return faSubProxy;
    }

    public void setFaSubProxy(String faSubProxy) {
        this.faSubProxy = faSubProxy;
    }

    public String getFaBisStatus() {
        return faBisStatus;
    }

    public void setFaBisStatus(String faBisStatus) {
        this.faBisStatus = faBisStatus;
    }

    public String getFaBusinessStatus() {
        return faBusinessStatus;
    }

    public void setFaBusinessStatus(String faBusinessStatus) {
        this.faBusinessStatus = faBusinessStatus;
    }

    public String getFaOblDefaultFlag() {
        return faOblDefaultFlag;
    }

    public void setFaOblDefaultFlag(String faOblDefaultFlag) {
        this.faOblDefaultFlag = faOblDefaultFlag;
    }

    public String getFaS35Flag() {
        return faS35Flag;
    }

    public void setFaS35Flag(String faS35Flag) {
        this.faS35Flag = faS35Flag;
    }

    public String getFaSpecificFlag() {
        return faSpecificFlag;
    }

    public void setFaSpecificFlag(String faSpecificFlag) {
        this.faSpecificFlag = faSpecificFlag;
    }

    public String getFaStructuredSpecificFlag() {
        return faStructuredSpecificFlag;
    }

    public void setFaStructuredSpecificFlag(String faStructuredSpecificFlag) {
        this.faStructuredSpecificFlag = faStructuredSpecificFlag;
    }

    public String getFaSnrProxy_() {
        return faSnrProxy_;
    }

    public void setFaSnrProxy_(String faSnrProxy_) {
        this.faSnrProxy_ = faSnrProxy_;
    }

    public String getFaSubProxy_() {
        return faSubProxy_;
    }

    public void setFaSubProxy_(String faSubProxy_) {
        this.faSubProxy_ = faSubProxy_;
    }
}

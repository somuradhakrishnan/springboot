package com.dbs.itt.sdwt.model;

import com.dbs.itt.sdwt.model.documents.Fa;
import com.dbs.itt.sdwt.model.documents.Gcin;
import com.dbs.itt.sdwt.model.documents.Mlc;
import com.dbs.itt.sdwt.model.documents.Murex;

/**
 * Created by ayyanars on 10/12/17.
 */
public class CounterParty {

    private String country;

    private String countryOfIncorp;

    private String industry;

    private String remark;

    private String mlcBcor;

    private String counterpartyName;

    private String lcinFullName;

    private Murex murexData;

    private Gcin gcinData;

    private Fa faData;

    private Mlc mlcData;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryOfIncorp() {
        return countryOfIncorp;
    }

    public void setCountryOfIncorp(String countryOfIncorp) {
        this.countryOfIncorp = countryOfIncorp;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getMlcBcor() {
        return mlcBcor;
    }

    public void setMlcBcor(String mlcBcor) {
        this.mlcBcor = mlcBcor;
    }

    public String getCounterpartyName() {
        return counterpartyName;
    }

    public void setCounterpartyName(String counterpartyName) {
        this.counterpartyName = counterpartyName;
    }

    public String getLcinFullName() {
        return lcinFullName;
    }

    public void setLcinFullName(String lcinFullName) {
        this.lcinFullName = lcinFullName;
    }

    public Murex getMurexData() {
        return murexData;
    }

    public void setMurexData(Murex murexData) {
        this.murexData = murexData;
    }

    public Gcin getGcinData() {
        return gcinData;
    }

    public void setGcinData(Gcin gcinData) {
        this.gcinData = gcinData;
    }

    public Fa getFaData() {
        return faData;
    }

    public void setFaData(Fa faData) {
        this.faData = faData;
    }

    public Mlc getMlcData() {
        return mlcData;
    }

    public void setMlcData(Mlc mlcData) {
        this.mlcData = mlcData;
    }
}

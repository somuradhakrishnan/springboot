package com.dbs.itt.sdwt.model.documents;

/**
 * Created by ayyanars on 10/12/17.
 */
public class Udf {

    private String hkKobIndustry;

    private String hkInstitution;

    private String hkChatsRtgs;

    private String hkCustomerRel;

    private String hkIndustry;

    private String twCounterpartyType;

    private String twRegNo;

    private String dbsiRelatedFlag;

    private String vnIndustry;

    private String vnOwnershipType;

    private String vnEntSize;

    private String vnSerIndustry;

    private String vnCreditInsForm;

    public String getHkKobIndustry() {
        return hkKobIndustry;
    }

    public void setHkKobIndustry(String hkKobIndustry) {
        this.hkKobIndustry = hkKobIndustry;
    }

    public String getHkInstitution() {
        return hkInstitution;
    }

    public void setHkInstitution(String hkInstitution) {
        this.hkInstitution = hkInstitution;
    }

    public String getHkChatsRtgs() {
        return hkChatsRtgs;
    }

    public void setHkChatsRtgs(String hkChatsRtgs) {
        this.hkChatsRtgs = hkChatsRtgs;
    }

    public String getHkCustomerRel() {
        return hkCustomerRel;
    }

    public void setHkCustomerRel(String hkCustomerRel) {
        this.hkCustomerRel = hkCustomerRel;
    }

    public String getHkIndustry() {
        return hkIndustry;
    }

    public void setHkIndustry(String hkIndustry) {
        this.hkIndustry = hkIndustry;
    }

    public String getTwCounterpartyType() {
        return twCounterpartyType;
    }

    public void setTwCounterpartyType(String twCounterpartyType) {
        this.twCounterpartyType = twCounterpartyType;
    }

    public String getTwRegNo() {
        return twRegNo;
    }

    public void setTwRegNo(String twRegNo) {
        this.twRegNo = twRegNo;
    }

    public String getDbsiRelatedFlag() {
        return dbsiRelatedFlag;
    }

    public void setDbsiRelatedFlag(String dbsiRelatedFlag) {
        this.dbsiRelatedFlag = dbsiRelatedFlag;
    }

    public String getVnIndustry() {
        return vnIndustry;
    }

    public void setVnIndustry(String vnIndustry) {
        this.vnIndustry = vnIndustry;
    }

    public String getVnOwnershipType() {
        return vnOwnershipType;
    }

    public void setVnOwnershipType(String vnOwnershipType) {
        this.vnOwnershipType = vnOwnershipType;
    }

    public String getVnEntSize() {
        return vnEntSize;
    }

    public void setVnEntSize(String vnEntSize) {
        this.vnEntSize = vnEntSize;
    }

    public String getVnSerIndustry() {
        return vnSerIndustry;
    }

    public void setVnSerIndustry(String vnSerIndustry) {
        this.vnSerIndustry = vnSerIndustry;
    }

    public String getVnCreditInsForm() {
        return vnCreditInsForm;
    }

    public void setVnCreditInsForm(String vnCreditInsForm) {
        this.vnCreditInsForm = vnCreditInsForm;
    }
}

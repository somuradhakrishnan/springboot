package com.dbs.itt.sdwt.model;

import com.dbs.itt.sdwt.model.documents.ValueObject;
import org.springframework.data.annotation.Id;

import java.util.List;

public class ConstantValues {

    @Id
    private String id;

    private String typeValue;

    private List<ValueObject> value;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getType() {
        return typeValue;
    }

    public void setType(String typeValue) {
        this.typeValue = typeValue;
    }


    public List<ValueObject> getValue() {
        return value;
    }

    public void setValue(List<ValueObject> value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ConstantValues{" +
                ", id='" + id + '\'' +
                ", typeValue=" + typeValue +
                ", value="+ value.toString() +
                '}';
    }

}

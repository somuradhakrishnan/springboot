package com.dbs.itt.sdwt.model.documents;

import java.util.List;

/**
 * Created by ayyanars on 10/12/17.
 */
public class Murex {

    private String murexLabel;

    private String cinDBSSG;

    private String cifDBSSG;

    private String cinNTDSG;

    private String cifNTDSG;

    private String cinDBSGH;

    private String cifDBSGH;

    private String hkBranchCif;

    private String hkTinNos;

    private String hkRmNumber;

    private String cifDHBHK;

    private String auCif;

    private String cifDBSLON;

    private String cifDBSMUM;

    private String indvcifDBSMUM;

    private String cifDBSSEO;

    private String cifDBSTPE;

    private String cifPTDBSI;

    private String cifDBSLAB;

    private String cifDHKMA;

    private String cifDBSCH;

    private String cifDBSHCM;

    private String cifDTBTPE;

    private String murexType;

    private String murexStatus;

    private DefaultAddress defaultDefaultAddress;

    private String reason;

    private String confirmationMode;

    private String relationship;

    private String classification;

    private String supDep1;

    private String ccpIndicator;

    private String cmConditionsMet;

    private String bankruptcyRemoteFlag;

    private String nbceIndicator;

    private String glAffiliate;

    private String clsEligibleCptyFlag;

    private String murexEntity;

    private String cmmSiPref;

    private String sendConfirmationFlag;

    private String paymentNettingFlag;

    private String notifyFixingFlag;

    private String fatcaStatus;

    private String supDep2;

    private String fatcaClause;

    private String comments;

    private String kyc;

    private String accreditedInvestorFlag;

    private String mepsRtgs;

    private String custodianSwift;

    private String counterpartyDTCCid;

    private String nonSwiftBei;

    private String brokerSaaCode;

    private String legalEntityRef;

    private String sector;

    private String cdea;

    private Udf udfFields;

    private List<SecondaryAddresses> secondaryAddressesList;

    public String getMurexLabel() {
        return murexLabel;
    }

    public void setMurexLabel(String murexLabel) {
        this.murexLabel = murexLabel;
    }

    public String getCinDBSSG() {
        return cinDBSSG;
    }

    public void setCinDBSSG(String cinDBSSG) {
        this.cinDBSSG = cinDBSSG;
    }

    public String getCifDBSSG() {
        return cifDBSSG;
    }

    public void setCifDBSSG(String cifDBSSG) {
        this.cifDBSSG = cifDBSSG;
    }

    public String getCinNTDSG() {
        return cinNTDSG;
    }

    public void setCinNTDSG(String cinNTDSG) {
        this.cinNTDSG = cinNTDSG;
    }

    public String getCifNTDSG() {
        return cifNTDSG;
    }

    public void setCifNTDSG(String cifNTDSG) {
        this.cifNTDSG = cifNTDSG;
    }

    public String getCinDBSGH() {
        return cinDBSGH;
    }

    public void setCinDBSGH(String cinDBSGH) {
        this.cinDBSGH = cinDBSGH;
    }

    public String getCifDBSGH() {
        return cifDBSGH;
    }

    public void setCifDBSGH(String cifDBSGH) {
        this.cifDBSGH = cifDBSGH;
    }

    public String getHkBranchCif() {
        return hkBranchCif;
    }

    public void setHkBranchCif(String hkBranchCif) {
        this.hkBranchCif = hkBranchCif;
    }

    public String getHkTinNos() {
        return hkTinNos;
    }

    public void setHkTinNos(String hkTinNos) {
        this.hkTinNos = hkTinNos;
    }

    public String getHkRmNumber() {
        return hkRmNumber;
    }

    public void setHkRmNumber(String hkRmNumber) {
        this.hkRmNumber = hkRmNumber;
    }

    public String getCifDHBHK() {
        return cifDHBHK;
    }

    public void setCifDHBHK(String cifDHBHK) {
        this.cifDHBHK = cifDHBHK;
    }

    public String getAuCif() {
        return auCif;
    }

    public void setAuCif(String auCif) {
        this.auCif = auCif;
    }

    public String getCifDBSLON() {
        return cifDBSLON;
    }

    public void setCifDBSLON(String cifDBSLON) {
        this.cifDBSLON = cifDBSLON;
    }

    public String getCifDBSMUM() {
        return cifDBSMUM;
    }

    public void setCifDBSMUM(String cifDBSMUM) {
        this.cifDBSMUM = cifDBSMUM;
    }

    public String getIndvcifDBSMUM() {
        return indvcifDBSMUM;
    }

    public void setIndvcifDBSMUM(String indvcifDBSMUM) {
        this.indvcifDBSMUM = indvcifDBSMUM;
    }

    public String getCifDBSSEO() {
        return cifDBSSEO;
    }

    public void setCifDBSSEO(String cifDBSSEO) {
        this.cifDBSSEO = cifDBSSEO;
    }

    public String getCifDBSTPE() {
        return cifDBSTPE;
    }

    public void setCifDBSTPE(String cifDBSTPE) {
        this.cifDBSTPE = cifDBSTPE;
    }

    public String getCifPTDBSI() {
        return cifPTDBSI;
    }

    public void setCifPTDBSI(String cifPTDBSI) {
        this.cifPTDBSI = cifPTDBSI;
    }

    public String getCifDBSLAB() {
        return cifDBSLAB;
    }

    public void setCifDBSLAB(String cifDBSLAB) {
        this.cifDBSLAB = cifDBSLAB;
    }

    public String getCifDHKMA() {
        return cifDHKMA;
    }

    public void setCifDHKMA(String cifDHKMA) {
        this.cifDHKMA = cifDHKMA;
    }

    public String getCifDBSCH() {
        return cifDBSCH;
    }

    public void setCifDBSCH(String cifDBSCH) {
        this.cifDBSCH = cifDBSCH;
    }

    public String getCifDBSHCM() {
        return cifDBSHCM;
    }

    public void setCifDBSHCM(String cifDBSHCM) {
        this.cifDBSHCM = cifDBSHCM;
    }

    public String getCifDTBTPE() {
        return cifDTBTPE;
    }

    public void setCifDTBTPE(String cifDTBTPE) {
        this.cifDTBTPE = cifDTBTPE;
    }

    public String getMurexType() {
        return murexType;
    }

    public void setMurexType(String murexType) {
        this.murexType = murexType;
    }

    public String getMurexStatus() {
        return murexStatus;
    }

    public void setMurexStatus(String murexStatus) {
        this.murexStatus = murexStatus;
    }

    public DefaultAddress getDefaultDefaultAddress() {
        return defaultDefaultAddress;
    }

    public void setDefaultDefaultAddress(DefaultAddress defaultDefaultAddress) {
        this.defaultDefaultAddress = defaultDefaultAddress;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getConfirmationMode() {
        return confirmationMode;
    }

    public void setConfirmationMode(String confirmationMode) {
        this.confirmationMode = confirmationMode;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public String getSupDep1() {
        return supDep1;
    }

    public void setSupDep1(String supDep1) {
        this.supDep1 = supDep1;
    }

    public String getCcpIndicator() {
        return ccpIndicator;
    }

    public void setCcpIndicator(String ccpIndicator) {
        this.ccpIndicator = ccpIndicator;
    }

    public String getCmConditionsMet() {
        return cmConditionsMet;
    }

    public void setCmConditionsMet(String cmConditionsMet) {
        this.cmConditionsMet = cmConditionsMet;
    }

    public String getBankruptcyRemoteFlag() {
        return bankruptcyRemoteFlag;
    }

    public void setBankruptcyRemoteFlag(String bankruptcyRemoteFlag) {
        this.bankruptcyRemoteFlag = bankruptcyRemoteFlag;
    }

    public String getNbceIndicator() {
        return nbceIndicator;
    }

    public void setNbceIndicator(String nbceIndicator) {
        this.nbceIndicator = nbceIndicator;
    }

    public String getGlAffiliate() {
        return glAffiliate;
    }

    public void setGlAffiliate(String glAffiliate) {
        this.glAffiliate = glAffiliate;
    }

    public String getClsEligibleCptyFlag() {
        return clsEligibleCptyFlag;
    }

    public void setClsEligibleCptyFlag(String clsEligibleCptyFlag) {
        this.clsEligibleCptyFlag = clsEligibleCptyFlag;
    }

    public String getMurexEntity() {
        return murexEntity;
    }

    public void setMurexEntity(String murexEntity) {
        this.murexEntity = murexEntity;
    }

    public String getCmmSiPref() {
        return cmmSiPref;
    }

    public void setCmmSiPref(String cmmSiPref) {
        this.cmmSiPref = cmmSiPref;
    }

    public String getSendConfirmationFlag() {
        return sendConfirmationFlag;
    }

    public void setSendConfirmationFlag(String sendConfirmationFlag) {
        this.sendConfirmationFlag = sendConfirmationFlag;
    }

    public String getPaymentNettingFlag() {
        return paymentNettingFlag;
    }

    public void setPaymentNettingFlag(String paymentNettingFlag) {
        this.paymentNettingFlag = paymentNettingFlag;
    }

    public String getNotifyFixingFlag() {
        return notifyFixingFlag;
    }

    public void setNotifyFixingFlag(String notifyFixingFlag) {
        this.notifyFixingFlag = notifyFixingFlag;
    }

    public String getFatcaStatus() {
        return fatcaStatus;
    }

    public void setFatcaStatus(String fatcaStatus) {
        this.fatcaStatus = fatcaStatus;
    }

    public String getSupDep2() {
        return supDep2;
    }

    public void setSupDep2(String supDep2) {
        this.supDep2 = supDep2;
    }

    public String getFatcaClause() {
        return fatcaClause;
    }

    public void setFatcaClause(String fatcaClause) {
        this.fatcaClause = fatcaClause;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getKyc() {
        return kyc;
    }

    public void setKyc(String kyc) {
        this.kyc = kyc;
    }

    public String getAccreditedInvestorFlag() {
        return accreditedInvestorFlag;
    }

    public void setAccreditedInvestorFlag(String accreditedInvestorFlag) {
        this.accreditedInvestorFlag = accreditedInvestorFlag;
    }

    public String getMepsRtgs() {
        return mepsRtgs;
    }

    public void setMepsRtgs(String mepsRtgs) {
        this.mepsRtgs = mepsRtgs;
    }

    public String getCustodianSwift() {
        return custodianSwift;
    }

    public void setCustodianSwift(String custodianSwift) {
        this.custodianSwift = custodianSwift;
    }

    public String getCounterpartyDTCCid() {
        return counterpartyDTCCid;
    }

    public void setCounterpartyDTCCid(String counterpartyDTCCid) {
        this.counterpartyDTCCid = counterpartyDTCCid;
    }

    public String getNonSwiftBei() {
        return nonSwiftBei;
    }

    public void setNonSwiftBei(String nonSwiftBei) {
        this.nonSwiftBei = nonSwiftBei;
    }

    public String getBrokerSaaCode() {
        return brokerSaaCode;
    }

    public void setBrokerSaaCode(String brokerSaaCode) {
        this.brokerSaaCode = brokerSaaCode;
    }

    public String getLegalEntityRef() {
        return legalEntityRef;
    }

    public void setLegalEntityRef(String legalEntityRef) {
        this.legalEntityRef = legalEntityRef;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getCdea() {
        return cdea;
    }

    public void setCdea(String cdea) {
        this.cdea = cdea;
    }

    public Udf getUdfFields() {
        return udfFields;
    }

    public void setUdfFields(Udf udfFields) {
        this.udfFields = udfFields;
    }

    public List<SecondaryAddresses> getSecondaryAddressesList() {
        return secondaryAddressesList;
    }

    public void setSecondaryAddressesList(List<SecondaryAddresses> secondaryAddressesList) {
        this.secondaryAddressesList = secondaryAddressesList;
    }
}

package com.dbs.itt.sdwt.repository;

import com.dbs.itt.sdwt.model.ConstantValues;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * Created by ayyanars on 10/12/17.
 */
public interface ConstantValuesRepository extends MongoRepository<ConstantValues, String>, QueryDslPredicateExecutor<ConstantValues> {
}

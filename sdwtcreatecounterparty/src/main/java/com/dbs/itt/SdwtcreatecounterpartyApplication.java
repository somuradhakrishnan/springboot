package com.dbs.itt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SdwtcreatecounterpartyApplication {

	public static void main(String[] args) {
		SpringApplication.run(SdwtcreatecounterpartyApplication.class, args);
	}
}

package com.myspace.demo.config.batch;

import org.apache.activemq.advisory.ConsumerEvent;
import org.apache.activemq.advisory.ConsumerListener;
import org.apache.activemq.broker.scheduler.JobListener;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.kahadb.util.ByteSequence;
import org.fusesource.hawtbuf.Buffer;
import org.fusesource.hawtbuf.UTF8Buffer;
import org.fusesource.mqtt.client.Listener;
import org.springframework.batch.core.*;
import org.springframework.batch.core.listener.MulticasterBatchListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.item.ItemWriter;
import org.springframework.boot.autoconfigure.jms.JmsProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.util.Arrays;
import java.util.EventListener;
import java.util.List;

/**
 * Created by ayyanars on 22/4/17.
 */
@Configuration
public class BatchConfiguration {

    private static final Log logger = LogFactory.getLog(BatchConfiguration.class);



    @Bean
    public ItemWriter<String> itemWriter(){
        //System.out.println(">>>>"+chunkProcessorChunkHandler.);
        ItemWriter<String> processItem = (List<? extends String> list) -> {};
        return processItem;
    }

    @Bean
    public Liste stepListener(){

        StepExecutionListener stepExecutionListener = new StepExecutionListener() {
            @Override
            public void beforeStep(StepExecution stepExecution) {
                System.out.println("M<MM<M<MM<MM<M111111");
            }

            @Override
            public ExitStatus afterStep(StepExecution stepExecution) {
                System.out.println("M<MM<M<MM<MM<M222222");
                return null;
            }
        };
        return stepExecutionListener;
    }

    @Bean
    public MulticasterBatchListener multicasterBatchListener(){
        MulticasterBatchListener batchListener = new MulticasterBatchListener();
        batchListener.setListeners(Arrays.asList(stepListener()));
        return batchListener;
    }
}

package com.myspace.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.integration.config.EnableIntegration;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})

@EnableIntegration
@ImportResource("demoSlavebatchContext.xml")
public class SpringbatchslavedemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbatchslavedemoApplication.class, args);
	}
}

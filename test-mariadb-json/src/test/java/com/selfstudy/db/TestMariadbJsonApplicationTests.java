package com.selfstudy.db;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import com.selfstudy.db.model.Attr;
import com.selfstudy.db.model.Products2;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestMariadbJsonApplicationTests {
	
	@PersistenceContext
	private EntityManager entityManager;

	@Test
	public void loadData() {
		List<Products2> products2List = new ArrayList<Products2>();
		for (int i = 0; i <= 1000; i++) {
			Attr attr = new Attr();
			attr.setJsonColumn1("jsonColumn" + i);
			attr.setJsonColumn2("jsonColumn" + i);
			attr.setJsonColumn3("jsonColumn" + i);
			attr.setJsonColumn4("jsonColumn" + i);
			attr.setJsonColumn5("jsonColumn" + i);
			attr.setJsonColumn6("jsonColumn" + i);
			attr.setJsonColumn7("jsonColumn" + i);
			attr.setJsonColumn8("jsonColumn" + i);
			attr.setJsonColumn9("jsonColumn" + i);
			attr.setJsonColumn10("jsonColumn" + i);

			Products2 product2 = new Products2();
			product2.setColundata1("colundata" + i);
			product2.setColundata2("colundata" + i);
			product2.setColundata3("colundata" + i);
			product2.setAttr(attr);
			products2List.add(product2);
		}
		
		bulkWithEntityManager(products2List);

	}

	public List<Products2> bulkWithEntityManager(List<Products2> products2List) {
		entityManager.getTransaction().begin();
		products2List.forEach(products2 -> entityManager.persist(products2));
		entityManager.getTransaction().commit();
		entityManager.close();

		return products2List;
	}

}

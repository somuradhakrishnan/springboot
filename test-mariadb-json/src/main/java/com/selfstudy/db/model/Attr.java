package com.selfstudy.db.model;

public class Attr {
	
	private String jsonColumn1;
	
	private String jsonColumn2;
	
	private String jsonColumn3;
	
	private String jsonColumn4;
	
	private String jsonColumn5;
	
	private String jsonColumn6;
	
	private String jsonColumn7;
	
	private String jsonColumn8;
	
	private String jsonColumn9;
	
	private String jsonColumn10;

	public String getJsonColumn1() {
		return jsonColumn1;
	}

	public void setJsonColumn1(String jsonColumn1) {
		this.jsonColumn1 = jsonColumn1;
	}

	public String getJsonColumn2() {
		return jsonColumn2;
	}

	public void setJsonColumn2(String jsonColumn2) {
		this.jsonColumn2 = jsonColumn2;
	}

	public String getJsonColumn3() {
		return jsonColumn3;
	}

	public void setJsonColumn3(String jsonColumn3) {
		this.jsonColumn3 = jsonColumn3;
	}

	public String getJsonColumn4() {
		return jsonColumn4;
	}

	public void setJsonColumn4(String jsonColumn4) {
		this.jsonColumn4 = jsonColumn4;
	}

	public String getJsonColumn5() {
		return jsonColumn5;
	}

	public void setJsonColumn5(String jsonColumn5) {
		this.jsonColumn5 = jsonColumn5;
	}

	public String getJsonColumn6() {
		return jsonColumn6;
	}

	public void setJsonColumn6(String jsonColumn6) {
		this.jsonColumn6 = jsonColumn6;
	}

	public String getJsonColumn7() {
		return jsonColumn7;
	}

	public void setJsonColumn7(String jsonColumn7) {
		this.jsonColumn7 = jsonColumn7;
	}

	public String getJsonColumn8() {
		return jsonColumn8;
	}

	public void setJsonColumn8(String jsonColumn8) {
		this.jsonColumn8 = jsonColumn8;
	}

	public String getJsonColumn9() {
		return jsonColumn9;
	}

	public void setJsonColumn9(String jsonColumn9) {
		this.jsonColumn9 = jsonColumn9;
	}

	public String getJsonColumn10() {
		return jsonColumn10;
	}

	public void setJsonColumn10(String jsonColumn10) {
		this.jsonColumn10 = jsonColumn10;
	}
	
	

}

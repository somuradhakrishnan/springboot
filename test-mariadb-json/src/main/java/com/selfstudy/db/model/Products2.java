package com.selfstudy.db.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "products2")
public class Products2 {

	private static final long serialVersionUID = -3009157732242241606L;
	@Id
	private long id;
	
	@Column(name = "colundata1")
	private String colundata1;
 
	@Column(name = "colundata2")
	private String colundata2;
	
	@Column(name = "colundata3")
	private String colundata3;
	
	@Type(type = "com.selfstudy.db.types.Json",
            parameters = {
                    @Parameter(
                            name = "classType",
                            value = "com.selfstudy.db.model.Attr"
                    )
            })
    @Column(name = "attr")
    private Attr attr;

	public String getColundata1() {
		return colundata1;
	}

	public void setColundata1(String colundata1) {
		this.colundata1 = colundata1;
	}

	public String getColundata2() {
		return colundata2;
	}

	public void setColundata2(String colundata2) {
		this.colundata2 = colundata2;
	}

	public String getColundata3() {
		return colundata3;
	}

	public void setColundata3(String colundata3) {
		this.colundata3 = colundata3;
	}

	public Attr getAttr() {
		return attr;
	}

	public void setAttr(Attr attr) {
		this.attr = attr;
	}
	
	
	
	
	
}

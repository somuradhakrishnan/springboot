package com.selfstudy.db;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestMariadbJsonApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestMariadbJsonApplication.class, args);
	}
}

package com.selfstudy.db.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.selfstudy.db.model.Products2;

public interface Product2Repo extends CrudRepository<Products2, Long>{

		List<Products2> findByColundata1(String Colundata1);
}

package com.selfstudy.db.repository;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.selfstudy.db.model.Products2;

public class Products2BulkImport extends SimpleJpaRepository<Products2, String>{
	
	private EntityManager entityManager;
    public Products2BulkImport(EntityManager entityManager) {
        super(Products2.class, entityManager);
        this.entityManager=entityManager;
    }
 
    @Transactional
    public List<Products2> save(List<Products2> items) {
        items.forEach(item -> entityManager.persist(item));
        return items;
    }

}

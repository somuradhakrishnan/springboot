package com.myspace.redisdemo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import redis.embedded.RedisServer;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.print.URIException;
import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Created by ayyanars on 23/4/17.
 */
@Component
public class EmbededRedis    {

    @Value("${spring.redis.port}")
    private int redisPort;

    private RedisServer redisServer;

    @PostConstruct
    public void startRedis() throws IOException,URISyntaxException {
        redisServer = new RedisServer(redisPort);
        redisServer.start();
    }

    @PreDestroy
    public void stopRedis() throws InterruptedException{
        redisServer.stop();
    }
}

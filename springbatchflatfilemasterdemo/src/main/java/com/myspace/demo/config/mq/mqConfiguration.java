package com.myspace.demo.config.mq;

import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.spring.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.integration.dsl.GenericEndpointSpec;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.core.Pollers;
import org.springframework.integration.dsl.jms.Jms;
import org.springframework.integration.jms.JmsSendingMessageHandler;

import javax.jms.Queue;

/**
 * Created by ayyanars on 16/4/17.
 */
@Configuration
public class MqConfiguration {


    @Value("${mq.request.queue}")
    private String queueName;

    @Bean
    public MessagingTemplate messagingGateway(){
        MessagingTemplate messagingTemplate = new MessagingTemplate(jmsRequests());
        messagingTemplate.setReceiveTimeout(5000);
        return  messagingTemplate;
    }

    @Bean
    public Queue testJMSQueue() {
        return new ActiveMQQueue(queueName);
    }


    @Bean
    public ActiveMQConnectionFactory connectionFactory(){
        ActiveMQConnectionFactory mqConnectionSource = new ActiveMQConnectionFactory();
        mqConnectionSource.setBrokerURL("tcp://localhost:61616");
        return mqConnectionSource;
    }

    @Bean
    public QueueChannel jmsRequests(){
        return new QueueChannel();
    }

    @Bean
    public QueueChannel replies(){
        return new QueueChannel();
    }


    @Bean
    public IntegrationFlow jmsOutboundFlow() {
        return IntegrationFlows.from(jmsRequests())
                .handleWithAdapter(h ->
                        h.jms(this.connectionFactory()).destination("testJMSQueue"),(GenericEndpointSpec<JmsSendingMessageHandler> spec) -> {
                        spec.poller(Pollers.fixedDelay(10000)
                                .receiveTimeout(0));
                })
                .get();
    }

    @Bean
    public IntegrationFlow jmsInboundFlow(){

        return IntegrationFlows.from(Jms.messageDrivenChannelAdapter(this.connectionFactory())
                .destination("replies")
                .outputChannel(replies())
                .autoStartup(false))
                .get();

    }

 }

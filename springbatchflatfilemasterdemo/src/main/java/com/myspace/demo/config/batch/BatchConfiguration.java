package com.myspace.demo.config.batch;

import com.myspace.demo.services.IteratortemProcessor;
import com.myspace.demo.services.listeners.JobstatusNotificationListener;
import com.myspace.demo.services.listeners.WriteMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.support.IteratorItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by ayyanars on 21/3/17.
 */

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

    private static final Logger log = LoggerFactory.getLogger(BatchConfiguration.class);

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    public WriteMonitor writeMonitor;

    @Value("classpath*:/sample-data*.csv")
    private Resource[] inputFiles;


    @Bean
    @StepScope
    public IteratorItemReader<String> reader(@Value("#{jobParameters['fileName'] ?: null}") String fileName) throws Exception{
        Stream<String> stream = Files.lines(Paths.get(fileName));
        IteratorItemReader<String> data = new IteratorItemReader(stream.iterator());
        return data;
    }


    @Bean
    public IteratortemProcessor processor() {
        return new IteratortemProcessor();
    }


    @Bean
    public TaskExecutor taskExecutor() {
        SimpleAsyncTaskExecutor asyncTaskExecutor=new SimpleAsyncTaskExecutor("spring_batch");
        asyncTaskExecutor.setConcurrencyLimit(600);
        return asyncTaskExecutor;
    }






    @Bean
    public ItemWriter<String> writer() {
        log.info("Intiate Writing");
        ItemWriter<String> writer = (List<? extends String> var1) -> System.out.println(var1);
        return writer;
    }

    @Bean
    public Job customJob(JobstatusNotificationListener listener) throws Exception{
        return jobBuilderFactory.get("customJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(step1())
                .end()
                .build();
    }



    @Bean
    public Step step1() throws Exception{
        return stepBuilderFactory.get("step1")
               .<String, String> chunk(600)
                //.reader(multiResourceItemReader())
                .reader(reader(null))
                .processor(processor())
                .writer(writer())
                .taskExecutor(taskExecutor())
                .build();
    }
}

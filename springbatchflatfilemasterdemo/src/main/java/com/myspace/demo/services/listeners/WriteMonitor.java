package com.myspace.demo.services.listeners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ItemReadListener;
import org.springframework.batch.core.ItemWriteListener;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by ayyanars on 14/4/17.
 */
@Component
public class WriteMonitor implements ItemWriteListener<String> {

    private static final Logger log = LoggerFactory.getLogger(WriteMonitor.class);

    @Override
    public void beforeWrite(List<? extends String> data) {
        //log.info("Reader Chunk Stared ");
    }

    @Override
    public void onWriteError(Exception var1,List<? extends String> data){
        log.error("Reader Chunk Error");
    }


    @Override
    public void afterWrite(List<? extends String> data) {

        // log.info("Reader Chunk Ended "+dat.getFirstName());
    }
}

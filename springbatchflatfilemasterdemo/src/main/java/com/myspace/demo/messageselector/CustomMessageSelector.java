package com.myspace.demo.messageselector;

import org.springframework.integration.core.MessageSelector;
import org.springframework.messaging.Message;

/**
 * Created by ayyanars on 27/5/17.
 */
public class CustomMessageSelector implements MessageSelector {

    @Override
    public boolean accept(Message<?> message) {
        System.out.println("Test Message Selector");
        return true;
    }
}

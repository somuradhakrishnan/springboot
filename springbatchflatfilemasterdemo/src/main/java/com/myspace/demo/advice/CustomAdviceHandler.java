package com.myspace.demo.advice;

import org.springframework.integration.handler.advice.AbstractRequestHandlerAdvice;
import org.springframework.messaging.Message;

/**
 * Created by ayyanars on 2/5/17.
 */
public class CustomAdviceHandler extends AbstractRequestHandlerAdvice {
    @Override
    protected Object doInvoke(ExecutionCallback executionCallback, Object o, Message<?> message) throws Exception {
        System.out.println("Get Message ID "+message.getPayload().toString());
        Object result = executionCallback.execute();
        return result;
    }
}

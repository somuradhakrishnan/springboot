package com.myspace.demo.services.listeners;

import com.myspace.demo.BatchConfiguration;
import com.myspace.demo.services.dao.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ItemReadListener;
import org.springframework.stereotype.Component;

import javax.batch.api.chunk.listener.ChunkListener;

/**
 * Created by ayyanars on 14/4/17.
 */
@Component
public class ReaderMonitor implements ItemReadListener<Person> {

    private static final Logger log = LoggerFactory.getLogger(ReaderMonitor.class);

    @Override
    public void beforeRead() {
        log.info("Reader Chunk Stared ");
    }

    @Override
    public void onReadError(Exception var1){
        log.error("Reader Chunk Error");
    }


    @Override
    public void afterRead(Person dat) {
        log.info("Reader Chunk Ended "+dat.getFirstName());
    }
}

package com.myspace.demo.services;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.repeat.RepeatStatus;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by ayyanars on 23/3/17.
 */
public class ReaderWriterTasklet<People> implements Tasklet{

    private ItemReader<People> reader;
    private ItemWriter<People> writer;
    // and corresponding setters

    public RepeatStatus execute(StepContribution contribution,
                                ChunkContext chunkContext) throws Exception {
        List<People> chunk = new LinkedList<People>();
        while (true) {
            People item = reader.read();
            if (item == null) {
                break;
            } else {
                chunk.add(item);
            }
        }
        writer.write(chunk);
        return RepeatStatus.FINISHED;
    }
}

package com.myspace.demo.services;

import com.myspace.demo.services.dao.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

/**
 * Created by ayyanars on 21/3/17.
 */
public class PersonItemProcessor implements ItemProcessor<Person, Person> {

    private static final Logger log = LoggerFactory.getLogger(PersonItemProcessor.class);

    @Override
    public Person process(final Person person) throws Exception {
        final String firstName = person.getFirstName().toUpperCase();
        final String lastName = person.getLastName().toUpperCase();
        final String personNumber = person.getPersonNumber();
        final Person transformedPerson = null;

        //log.info("Converting (" + person + ") into (" + transformedPerson + ")");

        return transformedPerson;
    }
}

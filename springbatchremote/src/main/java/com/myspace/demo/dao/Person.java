package com.myspace.demo.dao;

/**
 * Created by ayyanars on 27/3/17.
 */
public class Person {

    private String lastName;
    private String firstName;
    private String personNumber;

    public Person() {

    }

    public Person(String firstName, String lastName, String personNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.personNumber = personNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPersonNumber() { return personNumber; }

    public void setPersonNumber(String personNumber) { this.personNumber = personNumber; }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "firstName: " + firstName + ", lastName: " + lastName;
    }
}

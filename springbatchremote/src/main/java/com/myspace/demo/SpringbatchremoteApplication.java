package com.myspace.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.integration.config.EnableIntegration;

@SpringBootApplication
@EnableIntegration
@ImportResource("classpath*:/integartionXml/master-integration.xml")
public class SpringbatchremoteApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbatchremoteApplication.class, args);
	}
}

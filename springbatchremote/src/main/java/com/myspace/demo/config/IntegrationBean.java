package com.myspace.demo.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.jmx.ManagementContext;
import org.apache.activemq.hooks.SpringContextHook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.messaging.MessageChannel;

import java.util.Collections;

/**
 * Created by ayyanars on 27/3/17.
 */
@Configuration
public class IntegrationBean {

    @Value("${spring.activemq.broker-url}")
    private String brokerUrl;

    @Autowired
    private MessageChannel jmsRequests;




    @Bean
    public ActiveMQConnectionFactory connectionFactory(){
        return new ActiveMQConnectionFactory(brokerUrl);
    }

    @Bean
    public MessagingTemplate messagingGateway(){
        MessagingTemplate messagingTemplate = new MessagingTemplate(jmsRequests);
        messagingTemplate.setReceiveTimeout(5000);
        return  messagingTemplate;
    }

    @Bean( initMethod = "start", destroyMethod = "stop" )
    public BrokerService broker() throws Exception {
        final BrokerService broker = new BrokerService();
        broker.setBrokerName("localhost");
        broker.setUseJmx(false);
        broker.setTransportConnectorURIs(new String[] {"tcp://localhost:61616"});
        broker.setPersistent( false );
        broker.setShutdownHooks( Collections.< Runnable >singletonList( new SpringContextHook() ) );
        final ManagementContext managementContext = new ManagementContext();
        managementContext.setCreateConnector( true );
        broker.setManagementContext( managementContext );
        return broker;
    }

}

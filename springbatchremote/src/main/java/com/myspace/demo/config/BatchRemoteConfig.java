package com.myspace.demo.config;

import com.myspace.demo.dao.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.item.Chunk;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.support.ListItemReader;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import javax.sql.DataSource;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by ayyanars on 27/3/17.
 */
@Configuration
@EnableBatchProcessing
public class BatchRemoteConfig {

    private static final Logger log = LoggerFactory.getLogger(BatchRemoteConfig.class);

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public DataSource datasource;

    @Value("${mq.request.queue}")
    private String queueName;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Bean
    public ItemReader<Person> reader() {
        log.info("Intiate reading");

        ItemReader<Person> persons = null;
        try{
            Path file = Paths.get(ClassLoader.getSystemResource("sample-data.csv").toURI());
            Stream<String> lines = Files.lines( file, StandardCharsets.UTF_8 );
            persons = new ListItemReader<Person>(lines.map(mapToPerson).collect(Collectors.toList()));
        }catch (IOException | URISyntaxException e){
            e.printStackTrace();
        }
        return persons;
    }

    public static Function<String, Person> mapToPerson = (line) -> {
        String[] p = line.split(",");
        return new Person(p[0], p[1],p[2]);
    };

    @Bean
    public ItemWriter<Person> writer() {
        log.info("Intiate Writing");
        JdbcBatchItemWriter<Person> writer = new JdbcBatchItemWriter<Person>();
        writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<Person>());
        writer.setSql("INSERT INTO people (first_name, last_name, person_number) VALUES (:firstName, :lastName, :personNumber)");
        writer.setDataSource(datasource);
        return writer;
    }

    @Bean
    public Job importUserJob() {
        return jobBuilderFactory.get("importUserJob")
                .incrementer(new RunIdIncrementer())
                .flow(step1())
                .end()
                .build();
    }

    @Bean
    public Step step1() {
        return stepBuilderFactory.get("step1")
               .<Person, Person> chunk(100)
                .reader(reader())
                .writer(writer())
                .build();
    }


}

package com.myspace.demo.config;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.jms.Queue;

/**
 * Created by ayyanars on 27/3/17.
 */
@Configuration
public class RequestQueueConfig {

    @Value("${mq.request.queue}")
    private String queueName;

    @Bean
    public Queue testJMSQueue() {
        return new ActiveMQQueue(queueName);
    }
}

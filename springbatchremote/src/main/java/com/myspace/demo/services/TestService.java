package com.myspace.demo.services;

import org.springframework.stereotype.Service;

/**
 * Created by ayyanars on 27/3/17.
 */
@Service
public class TestService {

        public void testReq(String name) {
            System.out.println("################################");
            System.out.println("################################");
            System.out.println("################################");
            System.out.println("##  Hello " + name + "!!!");
            System.out.println("################################");
            System.out.println("################################");
            System.out.println("################################");
        }
}

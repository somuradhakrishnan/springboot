package com.myspace.demo;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import javax.jms.*;

import org.springframework.test.context.junit4.SpringRunner;

import static java.lang.Thread.sleep;


@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbatchremoteApplicationTests {

	@Autowired
	@Qualifier("connectionFactory")
    ActiveMQConnectionFactory connectionFactory;
	@Value("${mq.request.queue}")
	String queueName;
	Session session;
	Destination destination;
	MessageProducer producer;
	Connection conn;

	@Before
	public void setup(){

		try{
			conn = connectionFactory.createConnection();
			conn.start();
			session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
			destination = session.createQueue(queueName);
			this.producer = session.createProducer(destination);
			this.producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
		}catch(JMSException e){
			e.printStackTrace();
		}

	}

	@Test
	public void testMQBySendingReq() {

	    try{

            TextMessage txtMessage = session.createTextMessage();
            txtMessage.setText("Larry the Cable Guy");
            producer.send(destination, txtMessage);
            sleep(30000);


        }catch(JMSException | InterruptedException ex){
        ex.printStackTrace();

        }



	}


}
